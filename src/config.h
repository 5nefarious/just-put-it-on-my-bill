/* Power range for each motor. Allows motors to be tweaked individually */
static const Range LeftMotorRange = { -14, -50 };
static const Range RightMotorRange = { -14.1, -49 };

/* Angle range for servos */
static const Range LeftServoRange = { 553, 2318 };
static const Range RightServoRange = { 559, 2315 };

/* Robot dimensions (in) */
static const float wheelspan = 7 + 13.0 / 16;
static const float wheelsize = 3;

/* Number of encoder ticks in one rotation */
static const float countsperturn = 318;

/* Causes robot to over/underestimate amount moved in each cycle */
static const Range CountAdjustment = { 1, 1.10 };


/* Maximum waiting time (s) for the robot to start moving */
static const float startdelay = 60;

/* Navigation */
static const int maxtries = 2;
static const Range PositionTolerance = { 0.2, 1.0 };
static const Range HeadingTolerance = { 2.0, 10.0 };
static const float headingoffset = 1.5;
static const float maintainheading = 6.0;

/* Motion */
static const float steppower = 20;
static const int steptime = 35;
static const int stepdelay = 70;

#define straightbrake(d) ( \
	(fabs(d) < 5) ? pow(fabs(d) / 5, 2) : 1 \
)

#define turnbrake(a) ( \
	(fabs(a) < 135) ? pow(fabs(a) / 135, 3) : 1 \
)


/* Lift */
static const float liftoffset = 65;
static const float liftspeed = 1;



/* Define lights by voltage ranges */
static const Light StartLight = { 0.2, 0.8 };
static const Light RedLight = { 0.4, 0.6 };
static const Light BlueLight = { 0.8, 1.0 };

/* Define keyframe lists */

static const Keyframe mainramp[] = {
	/* x        y       heading     power */
	{ 30,       45,     270,        60 },
	{ 30,       24,     -1,         60 }
};

static const Keyframe sideramp[] = {

	{ 31,       24.3,   0,      60 },
	{ 44,       24.3,   -1,     20 },
	{ 44.5,     28,     90,     20 },
};

static const Keyframe dropoff[] = {

	{ 5.7,      47,     270,    80 },
};

static const Keyframe pickup[] = {

	{ 29.8,     14,     91,     80 },
};

static const Keyframe fuellight[] = {

	{ 29.5,     61,     270,    40 },
	{ 29.5,     63,     270,    10 }
};

static const Keyframe midswitch[] = {

	{ 6.6,      45,     90,     40 }
};

static const Keyframe finalbutton[] = {

	{ 2.5,      3.0,    -1,     40 }
};

/* Define order of tasks to be performed */

static const Task taskorder[] = {
	PICKUP_SUPPLIES,
	NAVIGATE_TOP,
	DROP_SUPPLIES,
	FUEL_PUMP_BUTTONS,
	TOGGLE_SWITCHES,
	NAVIGATE_BOTTOM,
	FINAL_BUTTON
};
