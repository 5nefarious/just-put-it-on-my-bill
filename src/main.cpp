#include <FEHIO.h>
#include <FEHLCD.h>
#include <FEHMotor.h>
#include <FEHRPS.h>
#include <FEHSD.h>
#include <FEHServo.h>
#include <FEHUtility.h>
#include <math.h>

/*
 * This code attempts to follow the style guide described here:
 * https://suckless.org/coding_style
 */

/* MACROS *********************************************************************/

#define TRUE            1
#define FALSE           0

#define PI              3.14159265358
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))
#define SIGN(x)         ((x > 0) - (x < 0))
#define SIGNCMPR(a,b)   ((a > 0) - (b > 0))

#define ALLOW_REVERSE   (1 << 0)

/* TYPES **********************************************************************/

/* Stores a state marker for the robot. */
typedef struct {
	float x;
	float y;
	float heading;
	float power;
	float angle;
} Keyframe;

/* Stores a min and max value. */
typedef struct {
	float min;
	float max;
} Range;

typedef Range Light;

/* Valid tasks */
typedef enum {
	PICKUP_SUPPLIES,
	NAVIGATE_TOP,
	DROP_SUPPLIES,
	TOGGLE_SWITCHES,
	FUEL_PUMP_BUTTONS,
	NAVIGATE_BOTTOM,
	FINAL_BUTTON
} Task;


/* Configuration file given access to macros and data types */
#include "config.h"


/* PROTOTYPE ******************************************************************/

static float distance(const Keyframe *k1, const Keyframe *k2);
static float fixangle(float angle);
static float headingto(const Keyframe *target);
static float anglediff(float h1, float h2);
static float angleto(float heading, unsigned int flags);
static float distanceto(const Keyframe *frame);
static float scaletorange(float a, Range r);

static void update(unsigned int flags);

static void setmotors(float left, float right);
static void stopmotors();
static void drive(float power);
static void driveto(float power, const Keyframe *frame);
static void drivet(float power, float time);
static void turn(float power);
static void turnto(float power, float heading, unsigned int flags);

static void navigate(const Keyframe *target);
static void pullframes(int framec, const Keyframe framev[]);

static void lift(float angle);
static void liftstep(float speed, float angle);
static void liftto(float angle);

static void printframe(const Keyframe *frame);
static void drawcds(unsigned int color);

static int checklight(Light light);
static void waitforlight(Light light, float delay);

static void pulltasks(int taskc, const Task taskv[]);


/* GLOBAL *********************************************************************/

/* Declare drivetrain motors */
static FEHMotor LeftMotor (FEHMotor::Motor0, 12.0);
static FEHMotor RightMotor (FEHMotor::Motor1, 12.0);

/* Shaft encoders for the motors */
static DigitalEncoder LeftEncoder (FEHIO::P1_1);
static DigitalEncoder RightEncoder (FEHIO::P1_0);

/* Servos for lift arm */
static FEHServo LeftServo (FEHServo::Servo6);
static FEHServo RightServo (FEHServo::Servo7);

/* CdS cell for reading lights on  the course */
static AnalogInputPin CdSCell (FEHIO::P0_0);

/* Current state of robot */
static Keyframe Robot = { 9, 9, 45, 0, 90 };
static float leftpower = 0;
static float rightpower = 0;

/* Stores the last position received from the RPS */
static Keyframe LastRPS;

static Keyframe Display;

/* Declare constants for calculations */
static const float countsperinch = countsperturn / (wheelsize * PI);
static const float inchestoheading = 360 / (PI * wheelspan);
static const float stepp = steppower / 100;


/* IMPLEMENTATION *************************************************************/

/*
 * Regular distance formula, except with keyframes.
 */
float
distance(const Keyframe *k1, const Keyframe *k2)
{
	return sqrt(pow(k2->x - k1->x, 2) + pow(k2->y - k1->y, 2));
}

/*
 * Fixes angle to be between 0 and 359.9 (positive)
 *
 * NOTE: This function contains modular arithmetic, which may be an expensive
 * operation. Do not use frequently.
 */
float
fixangle(float angle)
{
	if (angle > 0) {

		return fmod(angle, 360);

	} else {

		return 360 + fmod(angle, 360);
	}
}

/*
 * Returns the heading to a target from the current position.
 */
float
headingto(const Keyframe *target)
{
	return atan2(target->y - Robot.y, target->x - Robot.x) * 180 / PI;
}

/*
 * Returns the smallest angle between two headings. The first angle is
 * effectively subtracted from the second and the sign is preserved.
 */
float
anglediff(float h1, float h2)
{
	float a = fixangle(h2) - fixangle(h1);

	if (fabs(a) > 180) {

		return (a > 0) ? (a - 360) : (a + 360);

	} else {

		return a;
	}
}

/*
 * Returns the shortest angle to a given heading. Accepts the flag
 * ALLOW_REVERSE, which causes the function to also consider the heading directly
 * opposite to the target heading.
 */
float
angleto(float heading, unsigned int flags)
{
	float a = anglediff(Robot.heading, heading);

	if (flags & ALLOW_REVERSE) {

		if (fabs(a) > 90) {

			return (a > 0) ? (a - 180) : (a + 180);

		} else {

			return a;
		}
	}

	return a;
}

/*
 * Returns the straight distance to a keyframe. This is computed by multiplying
 * the actual distance by the cosine of the angle to the frame.
 *
 * NOTE: Using cosine means negative values can be returned. The sign of the
 * value indicates direction: positive is forward, negative is backward.
 */
float
distanceto(const Keyframe *frame)
{
	return distance(&Robot, frame) *
	       cos(angleto(headingto(frame), 0) * PI / 180);
}

/*
 * Scales value a (-1 to 1) to fit within range r, which is reflected about 0.
 *
 * Here's a rough graph of what that looks like:
 *
 *         ^     _
 *         |   _-
 *         | _-
 *         |-
 *         |
 * <--------------->
 *         |
 *       _-|
 *     _-  |
 *   _-    |
 *         v
 *
 * Note that zero is still zero.
 *
 */
float
scaletorange(float a, Range r)
{
	float value = (r.max - r.min) * fabs(a) + r.min;
	return value * SIGN(a);
}

/*
 * Mega-ugly function updates the robots position using data from the shaft
 * encoders. Uses leftpower and rightpower to figure out if the robot is
 * travelling perfectly straight or turning in place. Each of those cases is
 * handled separately. There is currently no code to handle any other type of
 * movement (pivoting, curved motion, etc.).
 *
 * The function also retreives RPS data and compares it with the last signal. If
 * the new RPS data is different, the robot's state frame is overwritten.
 */
void
update()
{
	/* Get counts from encoders */
	int leftcounts = LeftEncoder.Counts();
	int rightcounts = RightEncoder.Counts();

	/* Reset both encoders */
	LeftEncoder.ResetCounts();
	RightEncoder.ResetCounts();

	/* Use average from both encoders */
	float avgcount = (leftcounts + rightcounts) / 2.0 *
	                 scaletorange(Robot.power, CountAdjustment);

	/* Calculate inches travelled */
	float inches = avgcount / countsperinch;

	/* If the robot is moving in a straight line */
	if (leftpower == rightpower) {

		/* Account for direction */
		inches *= SIGN(leftpower);

		Robot.x += inches * cos(Robot.heading * PI / 180.0);
		Robot.y += inches * sin(Robot.heading * PI / 180.0);

		/* If the robot is turning */
	} else if (leftpower == rightpower * -1) {

		/* Get degrees turned */
		float degrees = inches * inchestoheading;

		/* Account for direction */
		degrees *= SIGN(rightpower);

		/* Update heading */
		Robot.heading += degrees;

		/* Normalize */
		Robot.heading = fixangle(Robot.heading);

		/* All other motion */
	} else {
		/* Ignore counts. */
	}

	/* Get data from RPS */
	Keyframe rps = { RPS.X(), RPS.Y(), RPS.Heading() };

	/* Check if RPS data is out of range */
	if (rps.x >= 0 && rps.y >= 0) {

		/* Check if RPS data has changed */
		if (rps.x - LastRPS.x != 0 || rps.y - LastRPS.y != 0 ||
				rps.heading - LastRPS.heading != 0) {

			/* Update last received RPS data */
			LastRPS.x = rps.x;
			LastRPS.y = rps.y;
			LastRPS.heading = rps.heading;

			/* Update robot state */
			Robot.x = LastRPS.x;
			Robot.y = LastRPS.y;
			Robot.heading = LastRPS.heading + headingoffset;
		}
	}

	SD.Printf("%f, %f, %f, %f, %f, %f\n", Robot.x, Robot.y, Robot.heading,
	          Robot.power * 100, Robot.angle, TimeNow());

	Display.power = Robot.power;
	Display.heading = anglediff(Robot.heading, 0);
	Display.angle = Robot.angle;
	printframe(&Display);
}

/*
 * Sets powers for each motor, scaled according to their ranges. Also updates
 * the leftpower and rightpower variables.
 * 
 * NOTE: Updating left/rightpower is important, because the update function uses
 * them to calculate the robot's new position.
 */
void
setmotors(float left, float right)
{
	leftpower = left;
	rightpower = right;

	LeftMotor.SetPercent(scaletorange(left, LeftMotorRange));
	RightMotor.SetPercent(scaletorange(right, RightMotorRange));
}

/*
 * Stops both motors. That's it. No, really. I wrote an entire function to save
 * two lines of code. And now I've added four lines of comments.
 */
void
stopmotors()
{
	LeftMotor.Stop();
	RightMotor.Stop();
}

/*
 * Moves the robot forward. Calculates distance travelled and updates the
 * robot's state.
 */
void
drive(float power)
{
	Robot.power = fabs(power);
	setmotors(power, power);
}

/*
 * Turns the robot in a given direction. Sign of the power determines direction
 * of turn: positive is anticlockwise, negative is clockwise.
 */
void
driveto(float power, const Keyframe *frame)
{
	float a, p, d = distanceto(frame);
	int step = 0;

	while (fabs(d) > PositionTolerance.min) {

		p = power * MAX(straightbrake(d), 0.01);

		if (fabs(d) <= PositionTolerance.max) {

			step = 1;
			p = stepp;

		} else {

			a = angleto(headingto(frame), 0);

			if (fabs(a) > maintainheading) {
				turnto(p, headingto(frame), 0);
			}

			step = 0;
		}

		drive(p * SIGN(d));

		if (step) {

			Sleep(steptime);
			stopmotors();
			Sleep(stepdelay);
		}

		update();

		d = distanceto(frame);
	}

	stopmotors();
}

/*
 * Sets equal power to both motors for specified amount of time.
 */
void
drivet(float power, float time)
{
	drive(power / 100);

	Sleep(time);
	stopmotors();
	Sleep(200);

	update();
}

/*
 * Turns the robot in a given direction. Sign of the power determines direction
 * of turn: positive is anticlockwise, negative is clockwise.
 */
void
turn(float power)
{
	Robot.power = fabs(power);
	setmotors(power * -1, power);
}

/*
 * Turns the robot to a specific heading. Starts by performing a fast, but
 * imprecise sweeping turn. Once the robot is within the maximum tolerance, it
 * begins a slower, more accurate stepwise turn. Function exits when robot is
 * within minimum tolerance.
 */
void
turnto(float power, float heading, unsigned int flags)
{
	float p, a = angleto(heading, flags);
	int step = 0;

	while (fabs(a) > HeadingTolerance.min) {

		p = power * MAX(turnbrake(a), 0.01);

		if (fabs(a) <= HeadingTolerance.max) {

			step = 1;
			p = stepp;

		} else {

			step = 0;
		}

		turn(p * SIGN(a));

		if (step) {

			Sleep(steptime);
			stopmotors();
			Sleep(stepdelay);
		}

		update();

		a = angleto(heading, flags);
	}

	stopmotors();
}

/*
 * Navigates to a keyframe. The robot first turns to the target and then drives
 * toward it. This is repeated until the robot's position matches within the
 * minimum tolerance, or until the maximum number of tries is exceeded. (Usually
 * the latter, if the QR code is not perfectly centered with the wheels).
 */
void
navigate(const Keyframe *target)
{
	/* Attempt to sync with RPS */
	update();

	float d = distance(&Robot, target);
	float power = target->power / 100;
	int tries = 0;

	while (d > PositionTolerance.min && tries < maxtries) {

		turnto(power, headingto(target), ALLOW_REVERSE);
		driveto(power, target);

		d = distance(&Robot, target);
		tries++;
	}

	if (target->heading >= 0) {

		turnto(power, target->heading, 0);
	}
}

/*
 * Pulls keyframes from an array and navigates to them in order. Returns control
 * to the main program once the end of the array is reached. Also displays a
 * little progressbar at the bottom of the screen, showing where it is in the
 * current list.
 */
void
pullframes(int framec, const Keyframe framev[])
{
	int unit = 320 / framec;

	/* While the frame list is not empty */
	for (int i = 0; i < framec; i++) {

		LCD.SetFontColor(WHITE);
		LCD.FillRectangle(i * unit, 235, unit, 5);

		/* Fetch the current frame */
		const Keyframe *pCurrentFrame = &framev[i];

		/* Navigate to selected frame */
		navigate(pCurrentFrame);

		LCD.SetFontColor(GRAY);
		LCD.FillRectangle(i * unit, 235, unit, 5);

		Sleep(200);
	}

	LCD.SetFontColor(BLACK);
	LCD.FillRectangle(0, 235, 320, 5);
}
/*
 * Sets the lift to a specified angle. NOTE: This is instantaneous and will
 * cause rapid lift movement. Use liftto for more controlled lift motion.
 */
void
lift(float angle)
{
	Robot.angle = angle;

	LeftServo.SetDegree(Robot.angle + liftoffset);
	RightServo.SetDegree(180 - Robot.angle - liftoffset);

	update();
}

/*
 * Moves the lift one step toward a specified angle from the current position.
 */
void
liftstep(float speed, float angle)
{
	lift(Robot.angle + SIGN(angle) * speed);
}

/*
 * Moves lift to an angle. Uses liftspeed to set the speed of motion.
 *
 * NOTE: Speed is greatly affected by the onscreen display. Disabling the
 * display will greatly speed up the lift.
 */
void
liftto(float angle)
{
	while (TRUE) {

		/* Get angle remaining for lift */
		float liftangle = angle - Robot.angle;

		/* Check if lift arm is in position */
		if (fabs(liftangle) <= liftspeed) {
			return;
		} else {

			/* Move lift in correct direction */
			liftstep(liftspeed, liftangle);
		}
	}
}

/*
 * Prints the state of a frame to the screen. The input frame is compared with
 * the last frame printed, and elements are only drawn when there is a
 * significant change from what is already on the screen.
 * 
 * Heading is drawn as a compass, while power and lift angle are drawn as bars.
 */
void
printframe(const Keyframe *frame)
{
	static Keyframe LastFrame = { 0 };

	if (fabs(anglediff(frame->heading, LastFrame.heading)) > 2) {
		LCD.SetFontColor(BLACK);
		int x = 100 + 50 * cos(LastFrame.heading * PI / 180);
		int y = 150 - 50 * sin(LastFrame.heading * PI / 180);
		LCD.FillCircle(x, y, 7);

		LCD.SetFontColor(WHITE);
		LCD.DrawCircle(100, 150, 50);
		x = 100 + 50 * cos(frame->heading * PI / 180);
		y = 150 - 50 * sin(frame->heading * PI / 180);
		LCD.FillCircle(x, y, 6);

		LastFrame.heading = frame->heading;
	} else {
		LCD.SetFontColor(WHITE);
	}

	if (fabs(frame->power - LastFrame.power) > 0.2) {
		LCD.DrawRectangle(198, 98, 24, 104);
		int p = 80 * frame->power;
		LCD.FillRectangle(200, 200 - p, 20, p);

		LastFrame.power = frame->power;
	}

	if (frame->angle != LastFrame.angle) {
		LCD.DrawRectangle(268, 98, 22, 104);
		float a = 40 * (frame->angle - 25) / 90;
		LCD.FillRectangle(270, MAX(140 - a, 140), 18, fabs(a));

		LastFrame.angle = frame->angle;
	}
}

/*
 * Draws a circle in the middle of the heading compass with the given color.
 * Supposed to be used to debug the CdS cell, but I don't know if this function
 * has ever worked.
 */
void
drawcds(unsigned int color)
{
	LCD.SetFontColor(color);
	LCD.FillCircle(100, 150, 30);
}

/*
 * Checks if the value from the CdS cell is within the range of the specified
 * light. Returns 1 if light is detected, returns 0 otherwise.
 */
int
checklight(Light light)
{
	if (CdSCell.Value() >= light.min && CdSCell.Value() <= light.max) {
		return 1;
	}

	return 0;
}

/*
 * Wait for the CdS to read a light. Takes the light (voltage range) and timeout
 * delay.
 */
void
waitforlight(Light light, float delay)
{
	/* Get the starting time */
	float timestart = TimeNow();

	/* Wait for a light */
	while (TimeNow() - timestart < delay) {

		/* Return if light is detected */
		if (checklight(light)) {
			return;
		}

		Sleep(100);
	}
}

/*
 * Pull tasks from the task list and execute them. Acceptable tasks are listed
 * in the Task enum type.
 */
void
pulltasks(int taskc, const Task taskv[])
{
	for (int i = 0; i < taskc; i++) {

		switch (taskv[i]) {
		case PICKUP_SUPPLIES:

			pullframes(1, pickup);

			liftto(-32);
			drivet(-10, 1);
			liftto(40);
			drivet(20, 2);

			break;

		case NAVIGATE_TOP:

			pullframes(3, sideramp);
			drivet(-20, 2);
			drivet(20, 1);
			drivet(100, 7);
			drivet(-10, 1.5);
			turnto(0.2, 140, 0);
			drivet(10, 0.5);
			turnto(0.2, 180, 0);
			drivet(40, 2);

			break;

		case DROP_SUPPLIES:

			pullframes(1, dropoff);
			drivet(-9, 1);
			lift(-40);
			Sleep(2.0);
			lift(90);
			drivet(20, 1);

			break;

		case TOGGLE_SWITCHES:

			pullframes(1, midswitch);
			liftto(0);
			drivet(-20, 2.0);
			Sleep(1.0);
			drivet(40, 1.0);

			break;

		case FUEL_PUMP_BUTTONS:

			pullframes(2, fuellight);

			while (TRUE) {
				if (checklight(RedLight)) {
					drawcds(RED);
					drivet(20, 1.0);
					liftto(-5);
					break;
				} else if (checklight(BlueLight)) {
					drawcds(BLUE);
					drivet(20, 1.0);
					liftto(-30);
					break;
				} else {
					Sleep(100);
				}
			}

			drivet(-10, 4.0);

			break;

		case NAVIGATE_BOTTOM:

			pullframes(2, mainramp);

			break;

		case FINAL_BUTTON:

			pullframes(1, finalbutton);

			break;

		default:

			LCD.WriteLine("Unknown task.");
		}
	}
}


/* PROGRAM ********************************************************************/

/*
 * Main entry point. Describes the procedure the robot will follow to complete
 * the course.
 */
int
main()
{
	/* Initialize RPS */
	RPS.InitializeTouchMenu();

	/* Start logging */
	SD.OpenLog();

	/* Reset shaft encoders */
	LeftEncoder.ResetCounts();
	RightEncoder.ResetCounts();

	/* Set servo limits */
	LeftServo.SetMin(LeftServoRange.min);
	LeftServo.SetMax(LeftServoRange.max);
	RightServo.SetMin(RightServoRange.min);
	RightServo.SetMax(RightServoRange.max);

	LCD.Clear();

	/* Start on light */
	waitforlight(StartLight, startdelay);

	/* Move off the starting platform */
	drivet(40, 1);

	/* Run tasks in order */
	pulltasks(7, taskorder);

	/* Stop logging */
	SD.CloseLog();

	return 0;
}
