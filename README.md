# Robot Code
This repository contains code for our robot. 

## Structure
Right now, `doc` contains just the pseudocode. Working Proteus code can be found
in the `src` directory. Most of the code is in `main.cpp`, with `config.h` storing
global constants and other parameters for easy access. `config.h` also
contains all of the keyframes.

## Team
* Justin Bright
* **Arvin Ignaci**
* Nicholas Lange
* K.C. Stower

Team F6, KAH 12:40

