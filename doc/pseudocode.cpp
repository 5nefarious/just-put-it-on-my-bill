#include <FEHIO.h>
#include <FEHMotor.h>
#include <FEHRPS.h>
#include <FEHServo.h>
#include <FEHUtility.h>

#define LENGTH(x)		(sizeof(x) / sizeof(x[0]))

/*
 * Stores the position and velocity (power) of the robot at a given point. Used
 * for positioning
 */
typedef struct {
    float x;
    float y;
    float heading;
    float power;
} Keyframe;

/* Configuration file containing global constants */
#include "pseudoconfig.h"

/* Declare drivetrain motors */
static FEHMotor leftmotor (FEHMotor::Motor0, 12.0);
static FEHMotor rightmotor (FEHMotor::Motor1, 12.0);

/* Shaft encoders for the motors */
static DigitalEncoder leftencoder (FEHIO::P1_0);
static DigitalEncoder rightencoder (FEHIO::P1_1);

/* CDS cell for reading lights on  the course */
static AnalogInputPin cdscell (FEHIO::P0_0);

/* TODO: Declare servos and other sensors */

/* Stores the current keyframe. Set to the starting frame initially. */
static int frameindex = startframe;

/* Current state of robot (position and heading) */
static Keyframe stateframe;

/*
 * Reads data from encoders and calculates current position using dead reckoning.
 * Checks with RPS for position data; data from RPS overrides calculation.
 */
void
update(void)
{
    /* Get counts */
    int leftcount = leftencoder.Counts();
    int rightcount = rightencoder.Counts();

    /* Reset encoders */
    leftencoder.ResetCounts();
    rightencoder.ResetCounts();

    /* TODO: Calculate current position */

    /* TODO: Get data from RPS */
}

/* 
 * Drives the robot according to inputs for each motor. Range for each motor
 * is -1 to 1, with -1 being full power backward and 1 being full power forward.
 *
 * NOTE: "Full power," as referenced here, refers to the power specified by the 
 * power levels for each motor (leftpower and rightpower).
 */
void
drive(float left, float right)
{
    leftmotor.SetPercent(left * leftpower);
    rightmotor.SetPercent(right * rightpower);
}

/*
 * Determines what powers to apply to the motors to get to the next keyframe.
 * Contains the core algorithm for movement.
 */
void
moveto(Keyframe *frame) {
    /* TODO */
}

/*
 * Applies power to motors to turn toward a particular heading.
 */
void
turnto(float heading) {
    /* TODO */
}

/*
 * Determines whether the robot is at a keyframe or not. If the robot is at
 * the correct position only, the function returns 1. If the robot is at
 * both the correct position and the correct heading, the function returns
 * 2. If neither of these is true, the function returns 0.
 *
 * NOTE: There is a level of tolerance as to how close the robot must be
 * in terms of position and heading. These tolerances are specified by
 * positiontolerance and headingtolerance.
 */
int
atframe(Keyframe *frame)
{
    /* TODO */
}

/*
 * Advances robot toward keyframe
 * 
 * NOTE: Keyframes have an associated power; the robot will attempt to be
 * travelling at that power when it reaches the keyframe.
 */
void
advance(Keyframe *frame)
{
    int ontarget = 0;

    /* Iterate while not on target */
    while (!ontarget) {

	/* If robot is at the keyframe */
	if (atframe(frame) == 2)
	    ontarget = 1

	/* If robot is at the correct position */
	else if (atframe(frame) == 1)
	    turnto(frame->heading);

	/* If robot is not at keyframe at all */
	else
	    moveto(frame);
	
	Sleep(waitdelay);

	update();
    }
}

/*
 * Continues moving through keyframes until a keyframe with 0 power is reached.
 * Returns control to the main program.
 */
void
runframes(void)
{
    int isnext = 1;

    /* Iterate while the next frame is valid */
    while (isnext) {

	/* Make sure frame exists */
	if (frameindex < LENGTH(keyframes)) {

	    /* Fetch the next frame */
	    Keyframe *currentframe = &keyframes[frameindex];

	    /* Go to the selected frame */
	    advance(currentframe);

	} else {
	    LCD.WriteLine("Invalid keyframe request.");
	}

	/* Check if power is 0 */
	if (currentframe->power != 0) {
	    isnext = 0;

	/* Check if this is the last keyframe */
	else if (frameindex >= LENGTH(keyframes)) {
	    isnext = 0; /* Don't pull any more frames */
	
	/* Otherwise, grab the next keyframe */
	} else {
	    frameindex++; /* Next index */
	    currentframe = &keyframes[frameindex];
	}
    }
}

/* PROGRAM ENTRY POINT */
/* 
 * This function describes the procedure the robot will follow to complete
 * the course.
 */
int
main(void)
{
    /* Get the starting time */
    float timestart = TimeNow();
    
    /* Wait for a green light */
    int greenlight = 0;
    while (!greenlight) {

	if (cdscell.Value() > cds_greenlight)
	    greenlight = 1;

	/* If green light is not detected, start moving after <maxwait> seconds */
	else if (TimeNow() - timestart > maxwait)
	    greenlight = 1;

	else
	    Sleep(waitdelay);
    }

    /* TODO: Lower forks */

    /* Go to supplies area */
    runframes();

    /* TODO */
    /*
	- Drive forward <x> number of inches, or until switches hit
	- Close claw to secure supplies
	- Set lift higher
	- Back up and turn 180 degrees
    */

    /* Go to switch area */
    runframes();

    /* TODO */
    /*
	- Set lift to maximum height
	- Drive forward <x> number of inches, or until switches hit
	- Open claw to drop supplies
	- Reverse out of dead zone
	- Set claw to open
	- Turn to align with switch
	- Drive foward until swich is in forklift.
	- Set claw to closed
	- Receive signal
	    - Drive forward or backward to toggle switch
	- Set claw to open
	- Back up and turn 90 degrees counterclockwise
	- Set forklift to a height halfway between buttons
    */

    /* Go to buttons */
    runframes();

    /* TODO */
    /*
	- Read light
	    - Move forklift up or down
	- Drive forward and hold position for 5 seconds
	- Back up and turn 180 degrees
    */

    /* Go back to start */
    runframes();

    /* TODO: Drive into button */
}
