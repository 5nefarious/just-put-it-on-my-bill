/* Maximum power (%) for each motor. Allows motors to be tweaked individually. */
static int leftpower = 40;
static int rightpower = 40;

/* Maximum value for a green light, as detected by the CDS cell. */
static float cds_greenlight = 2.7;

/* Maximum waiting time (s) for the robot to start moving */
static float maxwait 5;

/* Delay (ms) when waiting in an infinite loop */
static int waitdelay = 10;

/* Tolerances for determining if robot is at a keyframe */
static float positiontolerance = 0.05;
static float headingtolerance = 0.08;

/* Defines the keyframe on which the robot should start. This should be 0, unless
 * used for debugging. */
static int startframe = 0;

/* List of all keyframes the robot will need to pass through */
static Keyframe keyframes[] {
    /* List of keyframes for the first segment */
    { <x>, <y>, <heading>, 0 }, /* 0 power keyframe marks exit point */
    /* List of keyframes for second segment */
    /* ... */
}
